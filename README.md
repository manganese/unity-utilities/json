# Manganese - Unity Utilities - JSON

A wrapper around Newtonsoft's Json.NET library for inclusion in Unity.

## Installation

Add the Manganese Package Registry as a scoped registry to your Unity project's `manifest.json` file:

```
  "scopedRegistries": [
    {
      "name": "Manganese",
      "url": "http://unity.packages.mangane.se",
      "scopes": [
        "com.manganese"
      ]
    }
  ]
```

The Unity Package Manager should now find this package and give you the option to install it.

## Usage

Refer to the official [Json.NET documentation](https://www.newtonsoft.com/json/help/html/Introduction.htm).  This is only a wrapper to include a known working version in Unity.